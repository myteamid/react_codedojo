import { IState } from '../store/root-store';

export const lengthTodoDone = (state: IState): number => {
	let todoDone = state.todoApp.present.todoList.filter(item => item.completed);
	return todoDone.length;
};
