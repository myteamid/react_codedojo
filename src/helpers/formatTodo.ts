import { IItemTodo } from '../store/models';

interface TodoServer extends IItemTodo {
	_id: string;
}

export const formatTodo = (todo: TodoServer): IItemTodo => ({
	id: todo._id,
	title: todo.title,
	completed: todo.completed,
});
