import { IState } from '../store/root-store';

export const lengthTodoList = (state: IState): number => state.todoApp.present.todoList.length;
