import React, { ComponentClass, ChangeEvent, Component } from 'react'

interface State {
	valid: boolean,
	value: string
}

export const Validate = (WrappedComponent): ComponentClass => {
	return (
		class ValidateHOC extends Component {
			state: State = {
				valid: false,
				value: '',
			};

			private handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
				const value = event.target.value;
				this.setState({
					value,
					valid: !!value,
				})
			};

			private updateState = ({ value, valid }: State): void => {
				this.setState({
					value,
					valid,
				})
			};

			render() {
				const { valid, value } = this.state;
				return <WrappedComponent { ...this.props }
										 validate={ valid }
										 validateValue={ value }
										 updateState={ this.updateState }
										 handleValid={ this.handleChange } />
			}
		}
	)

	// return ValidateHOC;
};
