import { types } from '../../../constants';
import axios from 'axios';
import serverConfig from '../../../server-config.json';
import { IItemTodo } from '../../models';

export const loadTodoError = (bool: boolean, error: any): { type: string; error: any } => {
	console.log(error);
	return {
		type: types.FETCH_TODO_FAILURE,
		error: bool,
	};
};

export const loadTodoData = (data: [IItemTodo]): { type: string; todoList: [IItemTodo] } => ({
	type: types.FETCH_TODO_SUCCESS,
	todoList: data,
});

export const isLoadTodoList = (bool: boolean): { type: string; fetching: boolean } => ({
	type: types.FETCH_TODO_REQUEST,
	fetching: bool,
});

export const getTodoList = (): Function => {
	return dispatch => {
		dispatch(isLoadTodoList(true));

		setTimeout(() => {
			// dev only
			axios
				.get(`${serverConfig.apiPrefix}/${serverConfig.db.model}`)
				.then(response => {
					dispatch(isLoadTodoList(false));
					return response.data;
				})
				.then(response => dispatch(loadTodoData(response)))
				.catch(error => dispatch(loadTodoError(true, error)));
		}, 500);
	};
};
