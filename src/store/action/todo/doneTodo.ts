import axios from 'axios';
import { types } from '../../../constants';
import serverConfig from '../../../server-config.json';
import { IItemTodo } from '../../models';
import { isLoadTodoList, getTodoList, loadTodoError } from './loadTodo';

const actionDoneTodo = (id: any): { type: string; id: string } => ({
	type: types.DONE_TODO,
	id,
});

export const doneTodo = (item: IItemTodo): Function => {
	const { completed, id } = item;
	const body: { completed: boolean } = {
		completed: !completed,
	};

	return dispatch => {
		dispatch(isLoadTodoList(true));
		axios
			.post(`${serverConfig.apiPrefix}/${serverConfig.db.model}/${id}`, body)
			.then(() => isLoadTodoList(false))
			.then(() => dispatch(actionDoneTodo(id)))
			.then(() => dispatch(getTodoList()))
			.catch(error => dispatch(loadTodoError(true, error)));
	};
};
