import axios from 'axios';
import { types } from '../../../constants';
import serverConfig from '../../../server-config.json';
import { IItemTodo } from '../../models';
import { getTodoList, isLoadTodoList, loadTodoError } from './loadTodo';

const actionAddTodo = (item: IItemTodo): { type: string; newItem: IItemTodo } => ({
	type: types.ADD_TODO,
	newItem: item,
});

export function addTodo(text: string): Function {
	const newItem = {
		title: text,
		completed: false,
	};

	return dispatch => {
		dispatch(isLoadTodoList(true));

		axios
			.post(`${serverConfig.apiPrefix}/${serverConfig.db.model}`, newItem)
			.then(() => dispatch(isLoadTodoList(false)))
			.then(() => dispatch(actionAddTodo(newItem)))
			.then(() => dispatch(getTodoList()))
			.catch(error => dispatch(loadTodoError(true, error)));
	};
}
