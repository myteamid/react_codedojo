import axios from 'axios';
import { types } from '../../../constants';
import serverConfig from '../../../server-config.json';
import { loadTodoError, getTodoList, isLoadTodoList } from './loadTodo';

const actionChangeTodo = (id: string, text: string): { type: string; text: string; id: string } => ({
	type: types.CHANGE_TODO,
	text,
	id,
});

export const changeTodo = (id: string, text: string): Function => {
	const body = {
		title: text,
	};
	return dispatch => {
		dispatch(isLoadTodoList(true));
		axios
			.post(`${serverConfig.apiPrefix}/${serverConfig.db.model}/${id}`, body)
			.then(() => isLoadTodoList(false))
			.then(() => dispatch(actionChangeTodo(id, text)))
			.then(() => dispatch(getTodoList()))
			.catch(error => dispatch(loadTodoError(true, error)));
	};
};
