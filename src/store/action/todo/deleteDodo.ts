import axios from 'axios';
import { getTodoList, isLoadTodoList, loadTodoError } from './loadTodo';
import serverConfig from '../../../server-config.json';
import { types } from '../../../constants';

const actionDeleteTodo = (id: string): { type: string; id: string } => ({
	type: types.DELETE_TODO,
	id,
});

export const deleteTodo = (id: string): Function => {
	return dispatch => {
		dispatch(isLoadTodoList(true));
		axios
			.delete(`${serverConfig.apiPrefix}/${serverConfig.db.model}/${id}`)
			.then(() => isLoadTodoList(false))
			.then(() => dispatch(actionDeleteTodo(id)))
			.then(() => dispatch(getTodoList()))
			.catch(error => dispatch(loadTodoError(true, error)));
	};
};
