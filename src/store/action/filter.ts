import { types } from '../../constants';

export const filter = (value: string): { type: string; filter: string } => ({
  type: types.SET_FILTER,
  filter: value,
});
