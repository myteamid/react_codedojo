import { types } from '../../constants';

export const showModal = (bool: boolean): { type: string; open: boolean } => ({
  type: types.SHOW_MODAL,
  open: bool,
});

export const toggleAlert = (bool: boolean): { type: string; open: boolean } => ({
  type: types.TOGGLE_ALERT,
  open: bool,
});
