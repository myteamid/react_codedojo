import { types } from '../../constants';
import { formatTodo } from '../../helpers';

interface IInitialState {
	fetching: boolean;
	load: boolean;
	error: boolean;
	todoList: [];
}

const initialState: IInitialState = {
	fetching: false,
	load: false,
	error: false,
	todoList: [],
};

export const todos = (state = initialState, action): IInitialState => {
	switch (action.type) {
		case types.FETCH_TODO_REQUEST:
			return {
				...state,
				fetching: action.fetching,
			};

		case types.FETCH_TODO_SUCCESS:
			return {
				...state,
				load: true,
				todoList: action.todoList.map(formatTodo),
			};

		case types.FETCH_TODO_FAILURE:
			return {
				...state,
				load: false,
				error: action.error,
			};

		default:
			return state;
	}
};

//
// case types.ADD_TODO:
// return {
// 	...state,
// 	// newItem: [action.newItem]
// };
//
// case types.DELETE_TODO:
// return {
// 	...state,
// 	// deleteTodoId: action.id
// };
//
// case types.DONE_TODO:
// return {
// 	...state,
// 	// todoList: state.todoList.map(item =>
// 	//   item.id === action.id ? {...item, completed: !item.completed} : item)
// };
//
// case types.CHANGE_TODO:
// return {
// 	...state,
// 	// todoList: state.todoList.map(item =>
// 	//   item.id === action.id ? {...item, title: action.text} : item)
// };
