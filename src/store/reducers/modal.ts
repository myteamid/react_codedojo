import { types } from '../../constants';

interface IInitialState {
	dialog: boolean;
	alert: boolean;
}

const initialState: IInitialState = {
	dialog: false,
	alert: false,
};

export const modal = (state = initialState, action): IInitialState => {
	switch (action.type) {
		case types.SHOW_MODAL:
			return {
				...state,
				dialog: action.open,
			};

		// case types.DELETE_TODO:
		// 	return {
		// 		...state,
		// 		alert: false,
		// 	};

		case types.TOGGLE_ALERT:
			return {
				...state,
				alert: action.open,
			};

		default:
			return state;
	}
};
