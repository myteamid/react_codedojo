import { types } from '../../constants';

interface IInitialState {
	filteringTodo: string;
}

const initialState: IInitialState = {
	filteringTodo: types.FILTERING_ALL,
};

export const filtering = (state = initialState, action): IInitialState => {
	const { type, filter } = action;
	switch (type) {
		case types.SET_FILTER:
			return {
				...state,
				filteringTodo: filter,
			};
		default:
			return state;
	}
};
