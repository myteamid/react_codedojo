import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import { todos, modal, filtering } from './reducers';
import { IFilter, ITodoApp, IModal } from './models';
import undoable from 'redux-undo';
import thunk from 'redux-thunk';

const logger = createLogger();

const undoConfig = {};

export interface IState {
	todoApp: ITodoApp;
	modal: IModal;
	filter: IFilter;
}

const todoApp = combineReducers<IState>({
	todoApp: undoable(todos, undoConfig),
	modal,
	filter: filtering,
});

export const store = createStore(todoApp, composeWithDevTools(applyMiddleware(thunk, logger)));
