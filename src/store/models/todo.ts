export interface IItemTodo {
	id?: string;
	title: string;
	completed: boolean;
}
