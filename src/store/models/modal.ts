export interface IModal {
	dialog: boolean;
	alert: boolean;
}
