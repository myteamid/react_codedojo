import React, { ChangeEvent, ChangeEventHandler, FC, MouseEventHandler } from 'react';
import Grid from '@material-ui/core/Grid';
import { withStyles, createStyles, WithStyles, Theme } from '@material-ui/core/styles';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import { Modal } from '../Modal';
import { Snackbar } from '../Snackbar';
import { TodoList } from '../TodoList';
import { Filter } from '../Filter';

const styles = (theme: Theme) =>
	createStyles({
		inputStyles: {
			width: `${theme.spacing.unit * 30}px`,
		},
	});

interface IOwnProps {
	updateState: any;
	validateValue: boolean;
	handleValid?: any;
	showModal: any;
	addTodo: any;
	validate: boolean;
	modalOpen: boolean;
	classes: Record<'inputStyles', string>;
}

type Props = IOwnProps & WithStyles<typeof styles>;

const TodoGridComponent: FC<Props> = props => {
	const { updateState, validateValue, handleValid, showModal, addTodo, validate, modalOpen, classes } = props;

	const closeModalHandler: MouseEventHandler = event => {
		updateState({ value: '', valid: false });
		showModal(false);
	};

	const addTodoHandler = event => {
		event.preventDefault();
		addTodo(validateValue);
		closeModalHandler(event);
	};

	const handleChangeText: ChangeEventHandler = (event: ChangeEvent) => handleValid(event);

	return (
		<div>
			<Snackbar />
			<Grid container={true} justify="center">
				<Grid item={true} md={6}>
					<Filter />
					<TodoList />
				</Grid>
			</Grid>

			<Modal open={modalOpen} title={'Add new todo'} handleClose={closeModalHandler}>
				<form onSubmit={validate ? addTodoHandler : event => event.preventDefault()}>
					<DialogContent>
						<TextField
							className={classes.inputStyles}
							autoFocus={true}
							margin="dense"
							id="name"
							label="Todo title"
							type="text"
							onChange={handleChangeText}
							fullWidth={true}
						/>
					</DialogContent>
					<DialogActions>
						<Button onClick={closeModalHandler} color="primary">
							Cancel
						</Button>
						<Button onClick={addTodoHandler} disabled={!validate} color="primary">
							Add
						</Button>
					</DialogActions>
				</form>
			</Modal>
		</div>
	);
};

export const TodoGrid = withStyles(styles)(TodoGridComponent);
