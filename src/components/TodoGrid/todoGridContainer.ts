import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TodoGrid as TodoGridComponent } from './TodoGrid';
import { Validate } from '../../helpers';
import { showModal, addTodo } from '../../store/action';
import { IState } from '../../store/root-store';

const mapStateToProps = (state: IState) => ({
	modalOpen: state.modal.dialog,
});

const mapDispatchToProps = dispatch => ({
	showModal: bindActionCreators(showModal, dispatch),
	addTodo: bindActionCreators(addTodo, dispatch),
});

export const TodoGrid = connect(
	mapStateToProps,
	mapDispatchToProps
)(Validate(TodoGridComponent));
