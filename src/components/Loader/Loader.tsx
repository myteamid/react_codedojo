import React, { FC } from 'react';
import { withStyles, Theme, createStyles, WithStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = (theme: Theme) =>
	createStyles({
		row: {
			flexGrow: 1,
			position: 'fixed',
			width: '100%',
			left: 0,
			right: 0,
		},
		progress: {
			margin: `${theme.spacing.unit * 2}px auto`,
		},
	});

interface IOwnProps {
	classes: Record<'row' | 'progress', string>;
}

type Props = IOwnProps & WithStyles<typeof styles>;

const LoaderComponent: FC<Props> = props => {
	const { classes } = props;
	return (
		<div className={classes.row}>
			<LinearProgress color="secondary" />
		</div>
	);
};

export const Loader = withStyles(styles)(LoaderComponent);
