import React, { ChangeEvent, FC } from 'react';
import { withStyles, createStyles, Theme, WithStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import DoneAllOutlined from '@material-ui/icons/DoneAllOutlined';
import ViewList from '@material-ui/icons/ViewList';
import Badge from '@material-ui/core/Badge';
import { types } from '../../constants';

const styles = (theme: Theme) =>
	createStyles({
		root: {
			width: `${theme.spacing.unit * 30}px`,
			marginTop: `${theme.spacing.unit * 4}px`,
		},
		badge: {
			padding: `0 ${theme.spacing.unit * 2}px`,
			background: 'transparent',
		},
		active: {
			background: theme.palette.grey['300'],
		},
	});

interface IOwnProps {
	done: number;
	length: number;
	filteringTodo: string;
	filter: any;
	classes: Record<'active' | 'badge' | 'root', string>;
}

type Props = IOwnProps & WithStyles<typeof styles>;

const FilterComponent: FC<Props> = props => {
	const { done, length, filteringTodo, filter, classes } = props;

	const handleChange = (event: ChangeEvent<{}>, value: string) => filter(value);

	const renderLabelAll = (
		<Badge className={classes.badge} color="primary" badgeContent={length}>
			<ViewList />
		</Badge>
	);

	const renderLabelDone = (
		<Badge className={classes.badge} color="primary" badgeContent={done}>
			<DoneAllOutlined />
		</Badge>
	);

	return (
		<Grid container={true} justify="center">
			<BottomNavigation value={filteringTodo} onChange={handleChange} showLabels={true} className={classes.root}>
				<BottomNavigationAction
					classes={{ selected: classes.active }}
					label={renderLabelAll}
					value={types.FILTERING_ALL}
				/>
				<BottomNavigationAction
					classes={{ selected: classes.active }}
					label={renderLabelDone}
					value={types.FILTERING_COMPLETED}
				/>
			</BottomNavigation>
		</Grid>
	);
};

export const Filter = withStyles(styles)(FilterComponent);
