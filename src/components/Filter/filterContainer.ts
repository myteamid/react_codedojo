import { connect } from 'react-redux';
import { Filter as FilterComponent } from './Filter';
import { lengthTodoDone, lengthTodoList } from '../../helpers';
import { bindActionCreators } from 'redux';
import { filter } from '../../store/action';
import { IState } from '../../store/root-store';

const mapStateToProps = (state: IState) => ({
	done: lengthTodoDone(state),
	length: lengthTodoList(state),
	filteringTodo: state.filter.filteringTodo,
});

const mapDispatchToProps = dispatch => ({
	filter: bindActionCreators(filter, dispatch),
});

export const Filter = connect(
	mapStateToProps,
	mapDispatchToProps
)(FilterComponent);
