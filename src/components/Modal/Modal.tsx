import React, { FC } from 'react';
import { Theme, withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';

const style = (theme: Theme) => {
	console.log(theme);
	return {
		modalClass: {
			minWidth: `${theme.spacing.unit * 40}px`,
		},
	};
};

interface IProps {
	open: boolean;
	handleClose: any;
	title: string;
}

const ModalComponent: FC<IProps> = props => {
	const { open, handleClose, title } = props;

	return (
		<Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
			<DialogTitle id="form-dialog-title"> {title} </DialogTitle>
			{props.children}
		</Dialog>
	);
};

export let Modal = withStyles(style)(ModalComponent);
