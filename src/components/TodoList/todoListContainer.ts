import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TodoList as TodoItemComponent } from './TodoList';
import { getTodoList } from '../../store/action';
import { IState } from '../../store/root-store';
import { types } from '../../constants';

const mapStateToProps = ({ todoApp, filter }: IState) => {
	const filteredTodo = todoApp.present.todoList.filter(
		todo => filter.filteringTodo === types.FILTERING_ALL || todo.completed
	);
	return {
		todos: filteredTodo,
	};
};

const mapDispatchToProps = dispatch => ({
	getTodoList: bindActionCreators(getTodoList, dispatch),
});

export const TodoList = connect(
	mapStateToProps,
	mapDispatchToProps
)(TodoItemComponent);
