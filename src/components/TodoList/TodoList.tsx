import React, { Component } from 'react';
import List from '@material-ui/core/List';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { TodoItem } from '../TodoItem';
import { IItemTodo } from '../../store/models';

interface IProps {
	todos: [IItemTodo];
	getTodoList: any;
}

export class TodoList extends Component<IProps> {
	componentDidMount() {
		this.props.getTodoList();
	}

	render() {
		const { todos } = this.props;

		return (
			<List>
				<TransitionGroup>
					{todos.map(item => (
						<CSSTransition key={item.id} timeout={300} classNames="fade">
							<TodoItem item={item} />
						</CSSTransition>
					))}
				</TransitionGroup>
			</List>
		);
	}
}
