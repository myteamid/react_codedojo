import React, { FC, MouseEvent, MouseEventHandler } from 'react';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Add from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import { Loader } from '../Loader';

const styles = createStyles({
	margin: {
		marginLeft: 'auto',
	},
});

interface IOwnProps {
	showModal: any;
	loading: boolean;
	classes: Record<'margin', string>;
}

type Props = IOwnProps & WithStyles<typeof styles>;

const HeaderComponent: FC<Props> = props => {
	const { showModal, loading, classes } = props;

	const handleClick: MouseEventHandler = (event: MouseEvent) => showModal(true);

	return (
		<div>
			{loading ? <Loader /> : null}
			<Grid container={true} justify="center">
				<Grid item={true} md={6}>
					<AppBar position="static" color="inherit">
						<Toolbar>
							<Typography variant="h4" color={'textPrimary'}>
								TODO App
							</Typography>

							<IconButton
								color="primary"
								className={classes.margin}
								aria-label="Add"
								onClick={handleClick}
							>
								<Add fontSize="large" />
							</IconButton>
						</Toolbar>
					</AppBar>
				</Grid>
			</Grid>
		</div>
	);
};

export const Header = withStyles(styles)(HeaderComponent);
