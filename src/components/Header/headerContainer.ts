import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Header as HeaderComponent } from './Header';
import { showModal } from '../../store/action';
import { IState } from '../../store/root-store';

const mapStateToProps = ({ todoApp }: IState) => ({
	loading: todoApp.present.fetching,
});

const mapDispatchToProps = dispatch => ({
	showModal: bindActionCreators(showModal, dispatch),
});

export const Header = connect(
	mapStateToProps,
	mapDispatchToProps
)(HeaderComponent);
