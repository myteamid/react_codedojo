import { connect } from 'react-redux';
import { ActionCreators } from 'redux-undo';
import { bindActionCreators } from 'redux';
import { Snackbar as SnackbarComponent } from './Snackbar';
import { toggleAlert } from '../../store/action';
import { IState } from '../../store/root-store';

const mapStateToProps = ({ modal }: IState): { openAlert: boolean } => {
	// console.log(todoApp.past.length, 'length')
	return {
		openAlert: modal.alert,
		// canPast: todoApp.past.length > 0
	};
};

const mapDispatchToProps = dispatch => ({
	undo: () => dispatch(ActionCreators.undo()),
	toggleAlert: bindActionCreators(toggleAlert, dispatch),
});

export const Snackbar = connect(
	mapStateToProps,
	mapDispatchToProps
)(SnackbarComponent);
