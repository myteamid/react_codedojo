import React, { FC, MouseEventHandler, SyntheticEvent } from 'react';
import Button from '@material-ui/core/Button';
import SnackbarUi from '@material-ui/core/Snackbar';
import Fade from '@material-ui/core/Fade';

interface IProps {
	undo: any;
	toggleAlert: any;
	openAlert: any;
}

export const Snackbar: FC<IProps> = props => {
	const { undo, toggleAlert, openAlert } = props;

	const handleClick: MouseEventHandler = (): void => {
		undo();
		toggleAlert(false);
	};

	const handleClose = (event: SyntheticEvent, reason: string): void => {
		if (reason === 'timeout') {
			toggleAlert(false);
		}
	};

	const content = <span id="message-id">Are you sure you want to delete?</span>;

	const action = (
		<Button key="undo" color="secondary" size="small" onClick={handleClick}>
			{' '}
			UNDO{' '}
		</Button>
	);

	return (
		<div>
			<SnackbarUi
				anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
				open={openAlert}
				onClose={handleClose}
				autoHideDuration={3000}
				ContentProps={{ 'aria-describedby': 'message-id' }}
				TransitionComponent={Fade}
				message={content}
				action={[action]}
			/>
		</div>
	);
};
