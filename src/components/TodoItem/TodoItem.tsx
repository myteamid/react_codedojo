import React, { ChangeEvent, ChangeEventHandler, Component } from 'react';
import { createStyles, Theme, WithStyles, withStyles } from '@material-ui/core/styles';
import { TodoItemEdit } from './TodoItemEdit';
import { TodoItemDefault } from './TodoItemDefault';
import { IItemTodo } from '../../store/models';

const style = (theme: Theme) =>
	createStyles({
		doneItem: {
			background: theme.palette.grey[300],
			textDecoration: 'line-through',
			textDecorationColor: theme.palette.common.black,
		},
		item: {
			background: theme.palette.background.paper,
		},
	});

interface IOwnProps {
	item: IItemTodo;
	updateState: any;
	doneTodo: any;
	handleValid: any;
	deleteTodo: any;
	changeTodo: any;
	validateValue: string;
	validate: boolean;
	classes: Record<'doneItem' | 'item', string>;
}

type Props = IOwnProps & WithStyles<typeof style>;

interface IState {
	editTodo: boolean;
}

interface IClickHandlers {
	[key: string]: string;
}

class TodoItemComponent extends Component<Props, IState> {
	state = { editTodo: false };

	handleChange: ChangeEventHandler = (event: ChangeEvent) => this.props.handleValid(event);

	toggleDone = (item: IItemTodo) => this.props.doneTodo(item);

	openEdit = ({ title }: IItemTodo): void => {
		this.props.updateState(title, false);
		this.setState({ editTodo: true });
	};

	closeEdit = (): void => {
		this.props.updateState('', false);
		this.setState({ editTodo: false });
	};

	editDone = ({ id }: IItemTodo): void => {
		const { changeTodo, validateValue } = this.props;
		changeTodo(id, validateValue);
		this.setState({ editTodo: false });
	};

	delete = ({ id }: IItemTodo) => this.props.deleteTodo(id);

	clickHandlers: IClickHandlers = {
		toggleDone: 'toggleDone',
		openEdit: 'openEdit',
		closeEdit: 'closeEdit',
		editDone: 'editDone',
		delete: 'delete',
	};

	clickHandlerMap: object = {
		[this.clickHandlers.toggleDone]: this.toggleDone,
		[this.clickHandlers.openEdit]: this.openEdit,
		[this.clickHandlers.closeEdit]: this.closeEdit,
		[this.clickHandlers.editDone]: this.editDone,
		[this.clickHandlers.delete]: this.delete,
	};

	handleClick = (type, item: IItemTodo) => () => this.clickHandlerMap[type](item);

	render() {
		const { item, classes, validate } = this.props;
		const { editTodo } = this.state;
		const activeClass = item.completed ? classes.doneItem : classes.item;

		return (
			<React.Fragment>
				{editTodo ? (
					<TodoItemEdit
						handleChange={this.handleChange}
						handleClick={this.handleClick}
						clickHandlers={this.clickHandlers}
						item={item}
						validate={validate}
						activeClass={activeClass}
					/>
				) : (
					<TodoItemDefault
						handleClick={this.handleClick}
						clickHandlers={this.clickHandlers}
						item={item}
						activeClass={activeClass}
					/>
				)}
			</React.Fragment>
		);
	}
}

export const TodoItem = withStyles(style)(TodoItemComponent);
