import React, { FC, ChangeEventHandler } from 'react';
import ListItem from '@material-ui/core/ListItem';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import Close from '@material-ui/icons/Close';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import Done from '@material-ui/icons/Done';
import { createStyles, Theme, WithStyles, withStyles } from '@material-ui/core';
import { IItemTodo } from '../../store/models';

const style = (theme: Theme) =>
	createStyles({
		inputStyle: {
			marginRight: `${theme.spacing.unit * 10}px`,
			marginLeft: `${theme.spacing.unit + 6}px`,
		},
	});

type OwnProps = {
	item: IItemTodo;
	handleClick: any;
	handleChange: ChangeEventHandler;
	validate: boolean;
	activeClass: string;
	clickHandlers: { [key: string]: string };
	classes: Record<'inputStyle', string>;
};

type Props = OwnProps & WithStyles<typeof style>;

export const TodoItemEditComponent: FC<Props> = props => {
	const { activeClass, handleChange, handleClick, clickHandlers, validate, classes, item } = props;

	return (
		<ListItem className={activeClass} divider>
			<Checkbox tabIndex={1} disableRipple disabled checked={item.completed} />
			<Input
				onChange={handleChange}
				className={classes.inputStyle}
				defaultValue={item.title}
				fullWidth={true}
				inputProps={{ 'aria-label': 'Title' }}
			/>
			<ListItemSecondaryAction>
				<IconButton
					aria-label="Open Edit"
					onClick={handleClick(clickHandlers.editDone, item)}
					disabled={!validate}
				>
					<Done />
				</IconButton>
				<IconButton aria-label="Close Edit" onClick={handleClick(clickHandlers.closeEdit, item)}>
					<Close />
				</IconButton>
			</ListItemSecondaryAction>
		</ListItem>
	);
};

export const TodoItemEdit = withStyles(style)(TodoItemEditComponent);
