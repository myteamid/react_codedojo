import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TodoItem as TodoItemComponent } from './TodoItem';
import { Validate } from '../../helpers';
import { changeTodo, deleteTodo, doneTodo } from '../../store/action';

const mapDispatchToProps = dispatch => ({
	doneTodo: bindActionCreators(doneTodo, dispatch),
	changeTodo: bindActionCreators(changeTodo, dispatch),
	deleteTodo: bindActionCreators(deleteTodo, dispatch),
});

export const TodoItem = connect(
	null,
	mapDispatchToProps
)(Validate(TodoItemComponent));
