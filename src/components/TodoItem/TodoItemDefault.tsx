import React, { FC } from 'react';
import ListItem from '@material-ui/core/ListItem';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import { IItemTodo } from '../../store/models';

interface IProps {
	item: IItemTodo;
	handleClick: any;
	activeClass: string;
	clickHandlers: { [key: string]: string };
}

export const TodoItemDefault: FC<IProps> = props => {
	const { handleClick, item, activeClass, clickHandlers } = props;

	return (
		<ListItem
			className={activeClass}
			button={true}
			divider={true}
			onClick={handleClick(clickHandlers.toggleDone, item)}
		>
			<Checkbox tabIndex={1} disableRipple={true} checked={item.completed} />
			<ListItemText primary={item.title} />
			<ListItemSecondaryAction>
				<IconButton aria-label="Change" onClick={handleClick(clickHandlers.openEdit, item)}>
					<Edit />
				</IconButton>
				<IconButton aria-label="Delete" onClick={handleClick(clickHandlers.delete, item)}>
					<Delete />
				</IconButton>
			</ListItemSecondaryAction>
		</ListItem>
	);
};
