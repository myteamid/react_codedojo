import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const createMyTheme = createMuiTheme({
	typography: {
		useNextVariants: true,
	},
	spacing: {
		unit: 10,
	},
});

export const MuiTheme = props => <MuiThemeProvider theme={createMyTheme}> {props.children} </MuiThemeProvider>;
