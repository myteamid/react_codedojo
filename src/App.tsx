import React from 'react';
import { Header } from './components/Header';
import { TodoGrid } from './components/TodoGrid';
import { MuiTheme } from './MuiTheme';

export const App = () => {
	return (
		<MuiTheme>
			<div className="app">
				<Header />
				<TodoGrid />
			</div>
		</MuiTheme>
	);
};
