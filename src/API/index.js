// export const API_PATH = 'https://jsonplaceholder.typicode.com/todos?userId=1'
import config from '../server-config'
export const API_PATH = `${config.apiPrefix}/todos`
