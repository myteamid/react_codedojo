const mongoose = require('mongoose')
const Todo = require('../models/todo')
const config = require('../../server-config')

const mongoOptions = {
  useNewUrlParser: true
}

module.exports.setConnection = function setConnection () {
  mongoose.connect(`mongodb://${config.db.hosts}:${config.db.port}/${config.db.name}`, mongoOptions)
    .then((res) => console.log(`mongo connect DB name: ${config.db.name}`))
    .catch((e) => console.log(e))
}

module.exports.listFind = function listFind () {
  return Todo.find()
}

module.exports.createTodo = function createTodo (data) {
  const todo = new Todo({
    title: data.title,
    completed: data.completed
  })
  return todo.save()
}

module.exports.deleteTodo = function deleteTodo (id) {
  return Todo.findById(id).remove()
}

module.exports.updateItem = function updateItem (id, params) {
  const {completed, title} = params
  let set = {}

  if (params.hasOwnProperty('completed')) {
    set = {$set: {completed: completed} }
  }

  if (params.hasOwnProperty('title')) {
    set = { $set: {title: title} }
  }

  return Todo.update({"_id": id}, set)
}
