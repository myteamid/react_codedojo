const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const db = require('./utils/dbUtils')
const {serverPort} = require('../server-config')

const server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
const server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

db.setConnection()

const app = express()

app.use(bodyParser.json())
app.use(cors({origin: '*'}))

app.get('/todos', (req, res) => {
  db.listFind().then(data => {
    // console.log(data)
    res.send(data)
  })
})

app.post('/todos', (req, res) => {
  db.createTodo(req.body).then(data => res.send(data))
})

app.post('/todos/:id', (req, res) => {
  const {body, params} = req
  // console.log(req)
  db.updateItem(params.id, body).then(data => res.send(data))
})

app.delete('/todos/:id', (req, res) => {
  db.deleteTodo(req.params.id).then(data => res.send(data))
})

const server = app.listen(server_port, server_ip_address, () => {
  console.log(`server run on port: ${server_port}`)
})
