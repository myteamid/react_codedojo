const mongoose = require('mongoose')

const Schema = mongoose.Schema

const TodoScheme = new Schema({
  title: {type: String, require: true},
  completed: {type: Boolean, require: true}
})

const Todo = mongoose.model('Todo', TodoScheme)

module.exports = Todo